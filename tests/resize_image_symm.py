#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nibabel as nib
import matplotlib.pyplot as plt
import numpy as np

from dip_bias.funcs import resize_symm, unresize


image_fn = '/iacl/pg20/shuo/data/hippocampus/slices/Images/Penn001R_01_ss250.nii.gz'
image = nib.load(image_fn).get_data()
resized, sb, tb = resize_symm(image, [384*2, 256])
un = unresize(resized, image.shape, sb, tb)
print(np.sum(np.abs(un - image)))

plt.figure()
plt.subplot(1, 4, 1)
plt.imshow(image, cmap='gray')
plt.subplot(1, 4, 2)
plt.imshow(resized, cmap='gray')
plt.subplot(1, 4, 3)
plt.imshow(un, cmap='gray')
plt.subplot(1, 4, 4)
plt.imshow(np.abs(un - image), cmap='gray')
plt.show()
