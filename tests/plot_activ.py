#!/usr/bin/env python
# -*- coding: utf-8 -*-

import torch
import matplotlib.pyplot as plt


def func(x):
    ch1 = torch.nn.functional.relu(x)
    ch2 = 1 / (torch.nn.functional.relu(-x) + 1)
    print(ch2)
    return ch1 + ch2


x = torch.arange(-10, 10, 0.1)
y = func(x)
y1 = x + 1
y2 = 1 / (-x + 1)
# print(y)
plt.figure()
plt.plot(x, y, linewidth=8)
plt.plot(x, y1, '--y')
plt.text(-3, 2, r'$y = \frac{1}{1 - x}$')
plt.plot(x, y2, '--r')
plt.text(5, 4, r'$ y = x + 1$')
plt.grid(True)
plt.show()
