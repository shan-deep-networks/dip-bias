#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nibabel as nib
import matplotlib.pyplot as plt

from dip_bias.funcs import resize


image_fn = 'image.nii.gz'
image = nib.load(image_fn).get_data().squeeze()
resized1 = resize(image, [64, 64])[0]
resized2 = resize(image, [192, 192])[0]

plt.figure()
plt.subplot(1, 3, 1)
plt.imshow(image, cmap='gray')
plt.subplot(1, 3, 2)
plt.imshow(resized1, cmap='gray')
plt.subplot(1, 3, 3)
plt.imshow(resized2, cmap='gray')
plt.show()
