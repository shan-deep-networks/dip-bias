#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from noise_est_ICCV2015.noise_estimation import noise_estimate


image = np.load('data/image_noisy.npy')
image = (image - np.min(image)) / (np.max(image) - np.min(image))
print(np.min(image), np.max(image))
print(noise_estimate(image, 8) * 255)
