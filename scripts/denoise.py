#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Denoise with DIP.')
parser.add_argument('-i', '--input-image', required=True,
                    help='The input image to denoise.')
parser.add_argument('-o', '--output-image', required=True,
                    help='The output denoised image.')
parser.add_argument('-d', '--output-dir', default=None,
                    help='The directory for intermediate results.')
parser.add_argument('-s', '--image-shape', default=[256, 256], nargs=2,
                    type=int, help='The shape of the image to resize to.')
parser.add_argument('-n', '--num-iters', default=2000, type=int,
                    help='The number of total iterations.')
parser.add_argument('-b', '--num-burn-in', default=1000, type=int,
                    help='The number of burn-in iterations.')
parser.add_argument('-id', '--im-net-depth', default=6, type=int,
                    help='The number of pooling layers in image U-Net.')
parser.add_argument('-u', '--update-noise-std', default=1, type=float,
                    help='The Gaussian noise std for update gradients.')
parser.add_argument('-iv', '--im-param-var', default=1e-5, type=float,
                    help='The variance for image net input.')
parser.add_argument('-g', '--use-gpu', default=1, type=int,
                    help='Use GPU in denoising.')
parser.add_argument('-sl', '--save-losses', default=1, type=int,
                    help='Save the optimzation losses.')
parser.add_argument('-sm', '--save-images', default=1, type=int,
                    help='Save the itermediate images during optimization.')
parser.add_argument('-si', '--save-interval', default=10, type=int,
                    help='The saving interval for intermediate outputs.')
parser.add_argument('-sc', '--save-config', default=1, type=int,
                    help='Save the configurations.')
parser.add_argument('-ss', '--save-stats', default=1, type=int,
                    help='Save denoising stats.')
parser.add_argument('-ri', '--reference-image',
                    help='The reference image to evaluate denoising.')
parser.add_argument('-rm', '--reference-mask',
                    help='Evaluate the pixels inside this mask.')
parser.add_argument('-m', '--eval-method', choices=['mse'], default='mse',
                    help='The evaluation method.')
parser.add_argument('-w', '--write-at-end', default=0, type=int,
                    help='Write log at the end.')
parser.add_argument('-ni', '--norm-im', default=1, type=int,
                    help='Normalize image intensities.')
parser.add_argument('-nm', '--noise-est-mode', default='grada',
                    choices={'crop', 'fg', 'grad', 'cropa', 'grada'},
                    help='Normalize estimation mode.')
args = parser.parse_args()


import os
import json
import torch
import numpy as np
import nibabel as nib

from dip_bias.dip import Denoise
from dip_bias.observers import PrintDenoise, LogDenoise
from dip_bias.observers import SaveImage, SaveImageInterm
from dip_bias.observers import EvalImage, EvalImageInterm
from dip_bias.config import Config


Config.num_iters = args.num_iters
Config.num_burn_in = args.num_burn_in
Config.update_noise_std = args.update_noise_std
Config.im_net_depth = args.im_net_depth
Config.im_param_var = args.im_param_var
Config.use_gpu = bool(args.use_gpu)
Config.save_interval = args.save_interval
Config.norm_im = bool(args.norm_im)
Config.noise_est_mode = args.noise_est_mode
Config.show()

args.save_losses = (args.output_dir is not None) and args.save_losses
args.save_images = (args.output_dir is not None) and args.save_images
eval_ref = (args.output_dir is not None) and (args.reference_image is not None)

input_obj = nib.load(args.input_image)
header = input_obj.header
affine = input_obj.affine
image = input_obj.get_data()

denoise = Denoise(image)
denoise.register(PrintDenoise())
if args.save_losses:
    os.makedirs(args.output_dir, exist_ok=True)
    loss_fn = os.path.join(args.output_dir, 'loss.csv')
    denoise.register(LogDenoise(loss_fn, write_at_end=bool(args.write_at_end)))
if args.save_images:
    os.makedirs(args.output_dir, exist_ok=True)
    save = SaveImageInterm(args.output_dir, header=header, affine=affine)
    denoise.register(save)
if eval_ref:
    os.makedirs(args.output_dir, exist_ok=True)
    ref_image = nib.load(args.reference_image).get_data()
    ref_mask = None
    if args.reference_mask is not None:
        ref_mask = nib.load(args.reference_mask).get_data()
    eval_fn = os.path.join(args.output_dir, args.eval_method+'.csv')
    eval_im = EvalImageInterm(eval_fn, ref_image, ref_mask, args.eval_method,
                              write_at_end=bool(args.write_at_end))
    denoise.register(eval_im)
denoise.run()

output_image = denoise.get_image_output()
if output_image is not None:
    output_obj = nib.Nifti1Image(output_image, affine, header)
    output_obj.to_filename(args.output_image)

if args.save_config:
    os.makedirs(args.output_dir, exist_ok=True)
    config_fn = os.path.join(args.output_dir, 'config.json')
    Config.save_json(config_fn)

if args.save_stats:
    os.makedirs(args.output_dir, exist_ok=True)
    noise_fn = os.path.join(args.output_dir, 'stats.json')
    noise = float(denoise.image_noise_std)
    if Config.norm_im:
        noise = noise * denoise._im_std
    with open(noise_fn, 'w') as jfile:
        json.dump(dict(noise_est=noise), jfile, indent=4)
