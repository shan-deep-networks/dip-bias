#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Denoise with DIP.')
parser.add_argument('-i', '--input-image', required=True,
                    help='The input image to estimate bias field from.')
parser.add_argument('-io', '--image-output', required=True,
                    help='The output corrected image.')
parser.add_argument('-bo', '--bias-output', required=True,
                    help='The output bias field.')
parser.add_argument('-d', '--output-dir', default=None,
                    help='The directory for intermediate results.')
parser.add_argument('-s', '--image-shape', default=[256, 256], nargs=2,
                    type=int, help='The shape of the image to resize to.')
parser.add_argument('-n', '--num-iters', default=2000, type=int,
                    help='The number of total iterations.')
parser.add_argument('-b', '--num-burn-in', default=1000, type=int,
                    help='The number of burn-in iterations.')
parser.add_argument('-id', '--im-net-depth', default=6, type=int,
                    help='The number of pooling layers in image U-Net.')
parser.add_argument('-bd', '--bias-net-depth', default=2, type=int,
                    help='The number of pooling layers in bias U-Net.')
parser.add_argument('-bs', '--bias-size', default=[8, 8], type=int, nargs=2,
                    help='The spatial size of bias before upsampling.')
parser.add_argument('-nl', '--num-levels', default=1, type=int,
                    help='The number of output levels for bias.')
parser.add_argument('-u', '--update-noise-std', default=2, type=float,
                    help='The Gaussian noise std for update gradients.')
parser.add_argument('-iv', '--im-param-var', default=0.01, type=float,
                    help='The variance for image net input.')
parser.add_argument('-bv', '--bias-param-var', default=0.01, type=float,
                    help='The variance for bias net input.')
parser.add_argument('-g', '--use-gpu', default=1, type=int,
                    help='Use GPU in denoising.')
parser.add_argument('-sl', '--save-losses', default=1, type=int,
                    help='Save the optimzation losses.')
parser.add_argument('-sm', '--save-images', default=1, type=int,
                    help='Save the itermediate images during optimization.')
parser.add_argument('-si', '--save-interval', default=10, type=int,
                    help='The saving interval for intermediate outputs.')
parser.add_argument('-sc', '--save-config', default=1, type=int,
                    help='Save the configurations.')
parser.add_argument('-ss', '--save-stats', default=1, type=int,
                    help='Save denoising stats.')
parser.add_argument('-ri', '--reference-image',
                    help='The reference image to evaluate bias estimation.')
parser.add_argument('-rb', '--reference-bias',
                    help='The reference bias to evaluate bias estimation.')
parser.add_argument('-rim', '--reference-image-masks', nargs='+',
                    help='Evaluate the pixels inside this mask; comma sep.')
parser.add_argument('-rbm', '--reference-bias-masks', nargs='+',
                    help='Evaluate the pixels inside this mask; comma sep.')
parser.add_argument('-im', '--image-eval-methods', default=['mse'],
                    choices=['mse', 'nmse', 'ncc', 'ncc2', 'cv', 'cjv'],
                    nargs='+', help='The image evaluation methods.')
parser.add_argument('-bm', '--bias-eval-methods', default=['mse'],
                    choices=['mse', 'nmse', 'ncc', 'ncc2', 'cv', 'cjv'],
                    nargs='+', help='The bias evaluation methods.')
parser.add_argument('-w', '--write-at-end', default=0, type=int,
                    help='Write log at the end.')
parser.add_argument('-ni', '--norm-im', default=1, type=int,
                    help='Normalize image intensities.')
parser.add_argument('-nm', '--noise-est-mode', default='grada',
                    choices={'crop', 'fg', 'grad', 'cropa', 'grada', 'orig'},
                    help='Normalize estimation mode.')
parser.add_argument('-z', '--resize-mode', default='cent',
                    choices={'cent', 'symm'})
args = parser.parse_args()


import os
import json
import torch
import numpy as np
import nibabel as nib

from dip_bias.dip import EstimateBias
from dip_bias.observers import PrintBias, LogBias
from dip_bias.observers import SaveImage, SaveImageInterm
from dip_bias.observers import SaveBias, SaveBiasInterm
from dip_bias.observers import SaveBiasLevel, SaveBiasIntermLevel
from dip_bias.observers import EvalImage, EvalImageInterm
from dip_bias.observers import EvalBias, EvalBiasInterm
from dip_bias.observers import EvalBiasLevel, EvalBiasIntermLevel
from dip_bias.config import Config


Config.im_shape = args.image_shape
Config.num_iters = args.num_iters
Config.num_burn_in = args.num_burn_in
Config.update_noise_std = args.update_noise_std
Config.im_net_depth = args.im_net_depth
Config.im_param_var = args.im_param_var
Config.bias_param_var = args.bias_param_var
Config.num_levels = args.num_levels
Config.bias_net_depth = args.bias_net_depth
Config.bias_size = args.bias_size
Config.use_gpu = bool(args.use_gpu)
Config.save_interval = args.save_interval
Config.norm_im = bool(args.norm_im)
Config.noise_est_mode = args.noise_est_mode
Config.resize_mode = args.resize_mode
Config.show()

if args.num_levels == 1:
    SB = SaveBias
    SBI = SaveBiasInterm
    EB = EvalBias
    EBI = EvalBiasInterm
else:
    SB = SaveBiasLevel
    SBI = SaveBiasIntermLevel
    EB = EvalBiasLevel
    EBI = EvalBiasIntermLevel


args.save_losses = (args.output_dir is not None) and args.save_losses
args.save_images = (args.output_dir is not None) and args.save_images
args.write_at_end = bool(args.write_at_end)

eval_im = (args.output_dir is not None) and (args.reference_image is not None)
eval_bias = (args.output_dir is not None) and (args.reference_bias is not None)

input_obj = nib.load(args.input_image)
header = input_obj.header
affine = input_obj.affine
image = input_obj.get_data()

eb = EstimateBias(image)
eb.register(PrintBias())

if args.save_losses:
    os.makedirs(args.output_dir, exist_ok=True)
    loss_fn = os.path.join(args.output_dir, 'loss.csv')
    eb.register(LogBias(loss_fn, write_at_end=bool(args.write_at_end)))

if args.save_images:
    os.makedirs(args.output_dir, exist_ok=True)
    si = SaveImageInterm(args.output_dir, header=header, affine=affine)
    sb = SBI(args.output_dir, header=header, affine=affine)
    eb.register(si)
    eb.register(sb)

    si = SaveImage(args.output_dir, header=header, affine=affine)
    sb = SB(args.output_dir, header=header, affine=affine)
    eb.register(si)
    eb.register(sb)

if eval_im:
    assert len(args.reference_image_masks) == len(args.image_eval_methods)
    ref_image_masks = list()
    for m in args.reference_image_masks:
        if m.lower() == 'none':
            ref_image_masks.append(None)
        elif ':' in m:
            ref_image_masks.append([nib.load(mm).get_data() for mm in m.split(':')])
        else:
            print(m)
            ref_image_masks.append(nib.load(m).get_data())

    os.makedirs(args.output_dir, exist_ok=True)
    ref_image = nib.load(args.reference_image).get_data()
    for i, (im_mask, im_evalm) in \
            enumerate(zip(ref_image_masks, args.image_eval_methods)):
        eval_fn = 'eval-image-interm-%d_' % i + im_evalm + '.csv'
        eval_fn = os.path.join(args.output_dir, eval_fn)
        eval_im = EvalImageInterm(eval_fn, ref_image, im_mask, im_evalm,
                                  args.write_at_end)
        eb.register(eval_im)

        eval_fn = 'eval-image-%d_' % i + im_evalm + '.csv'
        eval_fn = os.path.join(args.output_dir, eval_fn)
        eval_im = EvalImage(eval_fn, ref_image, im_mask, im_evalm,
                            args.write_at_end)
        eb.register(eval_im)

if eval_bias:
    assert len(args.reference_bias_masks) == len(args.bias_eval_methods)
    ref_bias_masks = list()
    for m in args.reference_bias_masks:
        if m.lower() == 'none':
            ref_bias_masks.append(None)
        elif ':' in m:
            ref_bias_masks.append([nib.load(mm).get_data() for mm in m.split(':')])
        else:
            print(m)
            ref_bias_masks.append(nib.load(m).get_data())

    os.makedirs(args.output_dir, exist_ok=True)
    ref_bias = nib.load(args.reference_bias).get_data()
    for i, (bias_mask, bias_evalm) in \
            enumerate(zip(ref_bias_masks, args.bias_eval_methods)):
        eval_fn = 'eval-bias-interm-%d_' % i + bias_evalm + '.csv'
        eval_fn = os.path.join(args.output_dir, eval_fn)
        eval_bias = EBI(eval_fn, ref_bias, bias_mask, bias_evalm,
                        args.write_at_end)
        eb.register(eval_bias)

        eval_fn = 'eval-bias-%d_' % i + bias_evalm + '.csv'
        eval_fn = os.path.join(args.output_dir, eval_fn)
        eval_bias = EB(eval_fn, ref_bias, bias_mask, bias_evalm,
                       args.write_at_end)
        eb.register(eval_bias)

eb.run()

bias_output = eb.get_bias_output()
bias_output_obj = nib.Nifti1Image(bias_output, affine, header)
bias_output_obj.to_filename(args.bias_output)

image_output = eb.get_image_output()
image_output_obj = nib.Nifti1Image(image_output, affine, header)
image_output_obj.to_filename(args.image_output)

if args.save_config:
    os.makedirs(args.output_dir, exist_ok=True)
    config_fn = os.path.join(args.output_dir, 'config.json')
    Config.save_json(config_fn)

if args.save_stats:
    os.makedirs(args.output_dir, exist_ok=True)
    noise_fn = os.path.join(args.output_dir, 'stats.json')
    noise = float(eb.image_noise_std)
    if Config.norm_im:
        noise = noise * eb._im_std
    with open(noise_fn, 'w') as jfile:
        json.dump(dict(noise_est=noise), jfile, indent=4)
