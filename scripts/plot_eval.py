#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Plot the loss.')
parser.add_argument('-i', '--input-loss', required=True)
parser.add_argument('-o', '--output-figure', default=None)
args = parser.parse_args()


import os
import matplotlib.pyplot as plt
import pandas as pd


df = pd.read_csv(args.input_loss)
plt.plot(df['iter'], df['mse'])
plt.grid(True)
plt.show()
