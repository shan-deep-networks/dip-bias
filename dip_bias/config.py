# -*- coding: utf-8 -*-

from enum import Enum
from config import Config as Config_


class NoiseEstMode(str, Enum):
    """Enum of the noise estimation mode."""
    ORIG = 'orig'
    CROP = 'crop'
    FG = 'fg'
    GRAD = 'grad'
    GRADA = 'grada'
    CROPA = 'cropa'


class ResizeMode(str, Enum):
    """Enum of the image resizing mode."""
    CENT = 'cent'
    SYMM = 'symm'


class Config(Config_):

    im_lr = 5e-4
    """float: Adam initial learning rate for the image network."""

    bias_lr = 5e-4
    """float: Adam initial learning rate for the bias network."""
    
    im_param_var = 1e-5
    """float: Standard deviation of image network parameters."""

    bias_param_var = 10000
    """float: Standard deviation of bias network parameters."""

    update_noise_std = 1.0
    """float: Standard deviation of noise introduced to Adam updates."""

    num_burn_in = 7000
    """int: Number of burn-in iterations of optimization."""

    num_iters = 20000
    """int: Number of optimization iterations."""

    num_im_only_iters = 10
    """int: [0, :attr:`num_im_only_iters`) iterations during image net only
    optimiztion."""

    num_bias_only_iters = 20
    """int: [:attr:`num_bias_only_iters`, :attr:`num_im_only_iters`) during bias
    net only optimiztion."""

    im_net_depth = 6
    """int: Number of pooling layers of the UNet."""

    im_net_width = 128
    """int: Number of channels of the feature maps before the first pooling."""

    bias_net_depth = 3
    """int: Number of pooling layers of the UNet."""

    bias_net_width = 128
    """int: Number of channels of the feature maps before the first pooling."""

    bias_size = [8, 8]
    """int: Size of the output at the bottom level of the bias net."""

    num_levels = 4
    """int: Number of output levels used to predict the bias."""

    use_gpu = True
    """bool: Use GPU."""

    use_cat = False
    """bool: Use concatenation in UNet skips."""

    save_interval = 10
    """int: Interval to save outputs."""

    noise_est_shape = (128, 128)
    """tuple[int]: The cropping shape of the image for noise estimation."""

    im_shape = (256, 256)
    """tuple[int]: The the network input shape to resize the image to."""

    norm_im = True
    """bool: Normalize the image intensities with zscore."""

    noise_est_mode = 'grada'
    """str: Noise estimation mode. See :class:`NoiseEstMode` for choices."""

    resize_mode = 'cent'
    """str: Image resizing mode."""
