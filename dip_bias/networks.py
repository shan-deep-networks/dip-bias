# -*- coding: utf-8 -*-

import torch
import numpy as np
import warnings
from torch.nn.functional import interpolate

from pytorch_layers import create_two_upsample, create_k3_conv
from pytorch_unet.unet import UNet as _UNet
from pytorch_unet.blocks import ConvBlock


class ExpandingBlockSum(torch.nn.Module):
    """An exanpding block adding the shortcut to the input element-wise.

    Attributes:
        in_channels (int): The number of input channels.
        out_channels (int): The number of output channels.
        conv1 (pytorch_unet.block.ConvBlock): The first conv-norm-relu block.
        conv2 (pytorch_unet.block.ConvBlock): The second conv-norm-relu block.

    """
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv1 = ConvBlock(in_channels, out_channels)
        self.conv2 = ConvBlock(out_channels, out_channels)

    def forward(self, input, shortcut):
        output = input + shortcut
        output = self.conv1(output)
        output = self.conv2(output)
        return output


class ExpandingBlockCat(torch.nn.Module):
    """An exanpding block concatenating the shortcut to the input channel-wise.

    Attributes:
        in_channels (int): The number of input channels.
        out_channels (int): The number of output channels.
        conv1 (pytorch_unet.block.ConvBlock): The first conv-norm-relu block.
        conv2 (pytorch_unet.block.ConvBlock): The second conv-norm-relu block.

    """
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv1 = ConvBlock(2 * in_channels, out_channels)
        self.conv2 = ConvBlock(out_channels, out_channels)

    def forward(self, input, shortcut):
        output = torch.cat([input, shortcut], dim=1)
        output = self.conv1(output)
        output = self.conv2(output)
        return output


class UNet(_UNet):
    """A UNet for DIP.

    This UNet support:

    * Multi-level outputs.
    * Enforcing positivity of the outputs.
    * Upsampling the outputs with bspline.

    To enforce positivity, the conversion function is

    .. math:: 

        y = \begin{cases}
            1 / (1 - x)\quad & x < 0, \\
            x + 1\quad & x \geq 0.
        \end{cases}

    Note:
        One and only one of :attr:scale_factor and :attr:size should not be
        ``None`` for the target upsampling size of the outputs.

    Note:
        This network outputs a tensor if :attr:`num_levels` is a single scaler
        or a :class:`list` of tensors if :attr:`num_levels` is a :class:`list`.

    Attributes:
        num_trans_down (int): The number of pooling layers.
        num_channels (int): The number of channels in intermediate convolutions.
        output_levels (int or list[int]): The index/indices of the output
            expanding blocks (levels). The index is counted from top to bottom.
            It starts from 0 and should not be larger than
            :attr:`num_trans_down`.
        scale_factor (int or NoneType): The upsampling factor of the output.
        size (tuple[int] or NoneType): The target size of the upsampled output.
        normalize (bool): Normalize the output to be positive.
        use_cat (bool): Use concatenation between shortcuts and inputs in
            expanding blocks; otherwise, use summation.

    """
    def __init__(self, num_trans_down, num_channels, output_levels=0,
                 scale_factor=None, size=None, normalize=False, use_cat=False):
        if scale_factor is None and size is None:
            raise RuntimeError('scale_factor and size cannot be both None.')
        elif scale_factor is not None and size is not None:
            raise RuntimeError('scale_factor and size cannot be both not None.')

        self.scale_factor = scale_factor
        self.size = size
        self.normalize = normalize
        self.use_cat = use_cat
        super().__init__(num_channels, 1, num_trans_down, num_channels,
                         max_channels=num_channels, output_levels=output_levels)

    def _create_tu(self, in_channels, out_channels):
        return create_two_upsample()

    def _create_eb(self, in_channels, shortcut_channels, out_channels):
        if self.use_cat:
            return ExpandingBlockCat(in_channels, out_channels)
        else:
            return ExpandingBlockSum(in_channels, out_channels)

    def _create_out(self, in_channels):
        conv = create_k3_conv(in_channels, self.out_channels)
        return conv

    def forward(self, input):
        outputs = super().forward(input)
        if isinstance(outputs, dict):
            for k, v in outputs.items():
                mode = self._get_interp_mode(v)
                if self.scale_factor is not None:
                    scale_factor = self.scale_factor * (2 ** k)
                else:
                    scale_factor = None

                outputs[k] = interpolate(v, scale_factor=scale_factor,
                                         size=self.size, mode=mode)
                if self.normalize:
                    outputs[k] = self._normalize_output(outputs[k])
        else:
            mode = self._get_interp_mode(outputs)
            outputs = interpolate(outputs, scale_factor=self.scale_factor,
                                  size=self.size, mode=mode)
            if self.normalize:
                outputs = self._normalize_output(outputs)
        return outputs

    def _normalize_output(self, output):
        ch1 = torch.nn.functional.relu(output)
        ch2 = 1 / (torch.nn.functional.relu(-output) + 1)
        return ch1 + ch2

    def _get_interp_mode(self, output):
        if len(output.shape) == 4:
            mode = 'bicubic'
        elif len(output.shape) == 5:
            mode = 'trilinear'
            warnigns.warn('Use trilinear instead of tricubic')
        else:
            raise RuntimeError('Input size wrong.')
        return mode
