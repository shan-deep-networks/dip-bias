# -*- coding: utf-8 -*-

import torch
import numpy as np

from pytorch_layers import Config as LConfig
from noise_estimation import noise_estimate

from .config import Config, NoiseEstMode, ResizeMode
from .noise_adam import NoiseAdam
from .networks import UNet
from .funcs import resize, unresize, normalize, unnormalize, resize_symm


class Subject:
    """Notifies :class:`dip_bias.observers.Observer` when its status changed.

    """
    def __init__(self):
        self._observers = list()

    def _notify_update(self):
        for observer in self._observers:
            observer.update()

    def _notify_setup(self):
        for observer in self._observers:
            observer.setup()

    def _notify_cleanup(self):
        for observer in self._observers:
            observer.cleanup()

    def register(self, observer):
        """Registers an observer to notify."""
        observer.subject = self
        self._observers.append(observer)


class DIP(Subject):
    """Abstract class for DIP.

    Processing steps:

    1. Intensity normalization and cropping/padding (resizing).
    2. Processing with DIP (e.g. denoising).
    3. Renormalization and resizing.

    Note:
        For Bayesian inference, a modified Adam optimzer injecting Gaussian
        noise to the update gradient is used and the outputs of each iteration
        are averaged.

    Attributes:
        image (torch.Tensor): The image to process.
        image_noise_std (float): The estimated std of the additive Gaussian
            noise.
        im_net_input (torch.Tensor): The input to the image network.
        im_net (dip_bias.networks.UNet): A DIP network for image processing.
        im_mse (torch.nn.MSELoss): The likelihood term of the loss.
        iter (int): The current iteration index (zero-based).

    """
    def __init__(self, image):
        super().__init__()
        self.image = self._convert_image(image)
        self.image_noise_std = self._estimate_image_noise_std()
        print('Noise std', self.image_noise_std)
        self.im_net_input = self._create_im_net_input()
        self.im_net = self._create_im_net()
        self.im_mse = torch.nn.MSELoss(reduction='sum')

        self.iter = 0
        self._im_out = 0
        self._im_out_interm = None
        self._output_avg_counter = 0

    def _convert_image(self, image):
        assert len(image.shape) == 2 or len(image.shape) == 3
        self._source_shape = image.shape
        if Config.norm_im:
            self._im_std = np.std(image)
            image = normalize(image, self._im_std)
        if ResizeMode(Config.resize_mode) is ResizeMode.CENT:
            image, self._sbbox, self._tbbox = resize(image, Config.im_shape)
        elif ResizeMode(Config.resize_mode) is ResizeMode.SYMM:
            image, self._sbbox, self._tbbox = resize_symm(image, Config.im_shape)
        else:
            raise RuntimeError('Wrong resize mode.')
        image = torch.Tensor(image).float()[None, None, ...]
        return image.cuda() if Config.use_gpu else image

    def _estimate_image_noise_std(self):
        image = self.image.squeeze().detach().cpu().numpy()
        if NoiseEstMode(Config.noise_est_mode) is NoiseEstMode.ORIG:
            print('orig')
            noise_level = noise_estimate(image)
        elif NoiseEstMode(Config.noise_est_mode) is NoiseEstMode.CROP:
            cropped = resize(image, Config.noise_est_shape)[0]
            noise_level = noise_estimate(cropped)
        elif NoiseEstMode(Config.noise_est_mode) is NoiseEstMode.FG:
            noise_level = noise_estimate(image, fg=True)
        elif NoiseEstMode(Config.noise_est_mode) is NoiseEstMode.GRAD:
            noise_level = noise_estimate(image, fg=True, grad=True)
        elif NoiseEstMode(Config.noise_est_mode) is NoiseEstMode.CROPA:
            cropped = resize(image, Config.noise_est_shape)[0]
            noise_level = noise_estimate(cropped)
            noise_level = 1.09536053 * noise_level - 0.01145118
        elif NoiseEstMode(Config.noise_est_mode) is NoiseEstMode.GRADA:
            noise_level = noise_estimate(image, fg=True, grad=True)
            noise_level = 1.09843212 * noise_level - 0.01140139
        if noise_level < 0:
            noise_level = 0
        return noise_level

    def _create_im_net_input(self):
        shape = [1, Config.im_net_width] + list(self.image.shape[2:])
        result = torch.zeros(shape).float()
        result = result.cuda() if Config.use_gpu else result
        result.normal_(0, np.sqrt(Config.im_param_var)).requires_grad_()
        return result

    def _create_im_net(self):
        LConfig.dropout = 0
        LConfig.dim = self.image.dim() - 2
        LConfig.padding_mode = 'reflect'
        LConfig.interp_mode = 'cubic'
        depth = Config.im_net_depth
        width = Config.im_net_width
        net = UNet(depth, width, scale_factor=1, normalize=False,
                   use_cat=Config.use_cat)
        self._init_conv_weights(net)
        for name, param in net.named_parameters():
            if 'out' in name and 'bias' in name:
                print(name, param.data)
        return net.cuda() if Config.use_gpu else net

    def _init_conv_weights(self, net):
        for name, param in net.named_parameters():
            if param.dim() == self.image.dim(): # is conv weight
                d = np.prod(param.shape[2:])
                std = 1. / np.sqrt(param.shape[1] * d)
                print(param.shape, std)
                torch.nn.init.normal_(param, mean=0, std=std)
            elif name.split('.')[-1] == 'bias':
                torch.nn.init.zeros_(param)

    def _calc_net_weight_decay(self, net_width):
        kernel_size = net_width * (3 ** (self.image.dim() - 2))
        weight_decay = (self.image_noise_std ** 2) * kernel_size
        return weight_decay

    def _calc_input_weight_decay(self, var):
        return (self.image_noise_std ** 2) / var

    def _create_optim(self):
        raise NotImplementedError

    def run(self):
        """Runs the DIP processing."""
        raise NotImplementedError

    def get_image_output(self, post_proc=True):
        """Returns the averaged output image.
        
        Args:
            post_proc (bool): Perform intensity unnormalization and image
                resizing.

        Returns:
            numpy.ndarray: The averaged output.

        """
        image = None
        if self._output_avg_counter > 0:
            image = self._im_out.squeeze()
            image = self._post_proc_image(image) if post_proc else image
        return image

    def get_image_output_pt(self):
        """Returns the **un**averaged image output as a :class:`torch.Tensor`.
        
        No post-processing (intensity unnormalization or resizing) is performed.

        """
        return self._im_out_pt

    def get_image_output_interm(self, post_proc=True):
        """Returns output image of current iteration in :class:`numpy.ndarray`.

        Args:
            post_proc (bool): Perform intensity unnormalization and image
                resizing.

        Returns:
            numpy.ndarray: The current output image.

        """
        image = self._im_out_interm.squeeze()
        image = self._post_proc_image(image) if post_proc else image
        return image

    def _avg(self, prev, new, n):
        return (n - 1) / n * prev + 1 / n * new

    def _post_proc_image(self, image):
        if Config.norm_im:
            image = unnormalize(image, self._im_std)
        image = unresize(image, self._source_shape, self._sbbox, self._tbbox)
        return image


class Denoise(DIP):
    """Denoises an image using DIP.

    Attributes:
        optim (dip_bias.noise_adam.NoiseAdam): The optimizer. It optimizes both
            the input to the image network and the network parameters.

    """
    def __init__(self, image):
        super().__init__(image)
        self.optim = self._create_optim()
        print(self.optim)

    def _create_optim(self):
        im_in_wd = self._calc_input_weight_decay(Config.im_param_var)
        im_net_wd = self._calc_net_weight_decay(Config.im_net_width)
        return NoiseAdam([{'params': self.im_net.parameters(),
                           'weight_decay': im_net_wd},
                          {'params': self.im_net_input,
                           'weight_decay': im_in_wd}],
                         lr=Config.im_lr, update_std=Config.update_noise_std)

    def run(self):
        """Denoise the image.

        Returns:
            Denoise: The instance itself.

        """
        self._notify_setup()
        for self.iter in range(Config.num_iters):
            self.optim.zero_grad()
            self._im_out_pt = self.im_net(self.im_net_input)
            self._im_out_interm = self._im_out_pt.detach().cpu().numpy()
            mse_pt = self.im_mse(self._im_out_pt, self.image)
            self.loss = mse_pt.item()
            mse_pt.backward()
            self.optim.step()
            self._record_output()
            self._notify_update()
        self._notify_cleanup()
        return self

    def _record_output(self):
        if self.iter >= Config.num_burn_in:
            self._output_avg_counter += 1
            self._im_out = self._avg(self._im_out, self._im_out_interm,
                                     self._output_avg_counter)


class EstimateBias(DIP):
    """Estimates the bias field of an image.

    Attributes:
        bias_net_input (torch.Tensor): The input to the bias network.
        bias_net (dip_bias.networks.UNet): The network to estimate the bias.
        optim (dip_bias.noise_adam.NoiseAdam): The optimizer. It optimizes the
            inputs to both networks and the network parameters.
        level (int): The index of the current output level of the UNet. It
            counted from top to bottom.

    """
    def __init__(self, image):
        super().__init__(image)
        self.bias_net_input = self._create_bias_net_input()
        self.bias_net = self._create_bias_net()
        print(self.bias_net)
        self.im_optim = self._create_im_optim()
        self.bias_optim = self._create_bias_optim()
        print(self.im_optim)
        print(self.bias_optim)

        self.level = 0
        self._bias_out = 0

    def _create_bias_net_input(self):
        shape = Config.bias_size
        shape = [1, Config.bias_net_width] + shape
        result = torch.zeros(shape).float()
        result = result.cuda() if Config.use_gpu else result
        result.normal_(0, np.sqrt(Config.bias_param_var)).requires_grad_()
        return result

    def _create_bias_net(self):
        LConfig.dropout = 0
        LConfig.dim = self.image.dim() - 2
        LConfig.padding_mode = 'reflect'
        LConfig.interp_mode = 'cubic'
        depth = Config.bias_net_depth
        width = Config.bias_net_width
        output_levels = list(range(Config.num_levels))
        size = self.im_net_input.shape[2:]
        net = UNet(depth, width, output_levels=output_levels, size=size,
                   normalize=False, use_cat=Config.use_cat)
        self._init_conv_weights(net)
        for name, param in net.named_parameters():
            if 'out' in name and 'bias' in name:
                print(name, param.data)
        return net.cuda() if Config.use_gpu else net

    def _create_im_optim(self):
        im_in_wd = self._calc_input_weight_decay(Config.im_param_var)
        im_net_wd = self._calc_net_weight_decay(Config.im_net_width)
        return NoiseAdam([{'params': self.im_net.parameters(),
                           'weight_decay': im_net_wd, 'lr': Config.im_lr},
                          {'params': self.im_net_input,
                           'weight_decay': im_in_wd, 'lr': Config.im_lr}],
                         update_std=Config.update_noise_std)

    def _create_bias_optim(self):
        bias_in_wd = self._calc_input_weight_decay(Config.bias_param_var)
        bias_net_wd = self._calc_net_weight_decay(Config.bias_net_width)
        return NoiseAdam([{'params': self.bias_net.parameters(),
                           'weight_decay': bias_net_wd, 'lr': Config.bias_lr},
                          {'params': self.bias_net_input,
                           'weight_decay': bias_in_wd, 'lr': Config.bias_lr}],
                         update_std=Config.update_noise_std)

    def run(self):
        """Estimates the bias field of the image.

        Returns:
            Denoise: The instance itself.

        """
        self._notify_setup()
        for self.iter in range(Config.num_iters):
            for self.level in reversed(self.bias_net.output_levels):
                self.im_optim.zero_grad()
                self.bias_optim.zero_grad()

                self._im_out_pt = self.im_net(self.im_net_input)
                self._im_out_interm = self._im_out_pt.detach().cpu().numpy()
                self._bias_out_pt = self.bias_net(self.bias_net_input)
                if isinstance(self._bias_out_pt, dict):
                    self._bias_out_pt = self._bias_out_pt[self.level]

                self._bias_out_interm = self._bias_out_pt.detach().cpu().numpy()

                if self.iter < Config.num_im_only_iters:
                    mse_pt = self.im_mse(self._im_out_pt, self.image)
                    self.loss = mse_pt.item()
                    mse_pt.backward()
                    self.im_optim.step()

                elif self.iter < Config.num_bias_only_iters:
                    output_pt = self._im_out_pt.detach() * self._bias_out_pt
                    mse_pt = self.im_mse(output_pt, self.image)
                    self.loss = mse_pt.item()
                    mse_pt.backward()
                    self.bias_optim.step()

                else:
                    output_pt = self._im_out_pt * self._bias_out_pt
                    mse_pt = self.im_mse(output_pt, self.image)
                    self.loss = mse_pt.item()
                    mse_pt.backward()
                    self.im_optim.step()
                    self.bias_optim.step()

                self._record_outputs()
                self._notify_update()
        self._notify_cleanup()
        return self

    def get_bias_output(self, post_proc=True):
        """Returns the averaged output bias field.
        
        Args:
            post_proc (bool): Perform resizing to the output bias field.

        Returns:
            numpy.ndarray: The averaged output bias field.

        """
        bias = None
        if self._output_avg_counter > 0:
            bias = self._bias_out.squeeze()
            bias = self._post_proc_bias(bias) if post_proc else bias
        return bias

    def get_bias_output_pt(self):
        """Returns the current bias estimation in :class:`torch.Tensor`."""
        return self._bias_out_pt

    def get_bias_output_interm(self, post_proc=True):
        """Returns the current **un**averaged output bias field.
        
        Args:
            post_proc (bool): Perform resizing to the output bias field.

        Returns:
            numpy.ndarray: The estimated bias field at current iteration and
                level.

        """
        bias = self._bias_out_interm.squeeze()
        bias = self._post_proc_bias(bias) if post_proc else bias
        return bias

    def _post_proc_image(self, image):
        image = unresize(image, self._source_shape, self._sbbox, self._tbbox)
        return image

    def _post_proc_bias(self, bias):
        return self._post_proc_image(bias)

    def _record_outputs(self):
        if self.iter >= Config.num_burn_in and self.level == 0:
            self._output_avg_counter += 1
            self._im_out = self._avg(self._im_out, self._im_out_interm,
                                     self._output_avg_counter)
            self._bias_out = self._avg(self._bias_out, self._bias_out_interm,
                                       self._output_avg_counter)
