# -*- coding: utf-8 -*-
  
import numpy as np
from image_processing_3d import crop3d, uncrop3d, resize_bbox3d


def resize(image, target_shape):
    """Resizes an image by cropping or zero-padding around the centroid.

    Args:
        image (numpy.ndarray): The image to resize.
        target_shape (iterable): The target shape of the image.

    Returns
    -------
        resized: numpy.ndarray
            The resized image.
        source_bbox: tuple[slice]
            Bounding box of the area in the source image.
        target_bbox: tuple[slice]
            Bounding box of the area in the target image.
    
    """
    nimage = image - np.min(image)
    indices = [np.arange(s) for s in image.shape]
    meshgrid = np.meshgrid(*indices, indexing='ij')
    bbox = list()
    for grid, tshape in zip(meshgrid, target_shape):
        center = int(np.round(np.sum(grid * nimage) / np.sum(nimage)))
        start = center - tshape // 2
        stop = start + tshape
        bbox.append(slice(start, stop))
    if len(indices) == 2:
        bbox.append(slice(0, 1))
    image = image[..., None]
    resized, source_bbox, target_bbox = crop3d(image, tuple(bbox), pad='orig')
    if len(indices) == 2:
        resized = resized.squeeze()
    return resized, source_bbox, target_bbox


def resize_symm(image, target_shape):
    source_bbox = tuple(slice(0, s) for s in image.shape)
    top = (target_shape[0] - image.shape[0]) // 2
    bot = target_shape[0] - image.shape[0] - top
    left = (target_shape[1] - image.shape[1]) // 2
    right = target_shape[1] - image.shape[1] - left
    padded = np.pad(image, ((top, bot), (left, right)), 'symmetric')
    target_bbox = (slice(top, top+image.shape[0]),
                   slice(left, left+image.shape[1]))
    return padded, source_bbox, target_bbox


def unresize(image, source_shape, source_bbox, target_bbox):
    """Unresizes an image."""
    orig_dim = image.ndim
    if orig_dim == 2:
        image = image[..., None]
        source_shape = list(source_shape) + [1]
    result = uncrop3d(image, source_shape, source_bbox, target_bbox)
    if orig_dim == 2:
        result = result[:, :, 0]
    return result


def normalize(image, std):
    return image / std


def unnormalize(image, std):
    return image * std
