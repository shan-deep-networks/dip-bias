# -*- coding: utf-8 -*-

import os
import torch
import numpy as np
import nibabel as nib
import threading
from queue import Queue

from .config import Config
from .funcs import unresize


class Observer:
    """Observes :class:`dip_bias.dip.Subject`.

    * Call :meth:`update` within an iteration.
    * Call :meth:`setup` before the optimization.
    * Call :meth:`cleanup` after the optimization.

    Attributes:
        subject (dip_bias.dip.Subject): The subject to observe.

    """
    def __init__(self):
        self.subject = None

    def update(self):
        pass

    def setup(self):
        pass

    def cleanup(self):
        pass


def pad_number(num, total_num):
    """Pads integer with zeros.
    
    Args:
        num (int): The integer to pad.
        total_num (int): The largest possible integer.

    Returns:
        str: The padded integer.
    
    """
    digits = len(str(total_num))
    return ('%%0%dd' % digits) % num


class Print(Observer):
    """Prints optimization.
    
    """
    def sprint_loss(self):
        """Prints loss as :class:`str`."""
        return self.sprint_scientific(self.subject.loss)

    def sprint_torch_min_max(self, value):
        """Prints min and max from a :class:`torch.Tensor` as :class:`str`.
        
        Args:
            value (torch.Tensor): The value to calculate min and max from.

        Returns:
            str: The min and max in a format of `[min, max]`.
        
        """
        min = torch.min(value).item()
        max = torch.max(value).item()
        result = ', '.join([self.sprint_float(min), self.sprint_float(max)])
        result = '[' + result + ']'
        return result

    def sprint_float(self, value, decimals=2):
        """Prints :class:`float` as :class:`str` with a number of decimals.

        Args:
            value (float): The number to print.
            decimals (int): The number of decimals to print.

        Returns:
            str: The formated number.
        
        """
        return ('%%.%df' % decimals) % value

    def sprint_scientific(self, value, decimals=2):
        """Prints :class:`float` in scientific notation.

        Args:
            value (float): The value to print.
            decimals (int): The number of decimals to print.

        Returns:
            str: The formated number.
        
        """
        return ('%%.%de' % decimals) % value

    @property
    def iter(self):
        """Returns the current iteration index in a zero-padded format."""
        return pad_number(self.subject.iter, Config.num_iters - 1)


class PrintDenoise(Print):
    """Prints the optimization for :class:`dip_bias.dip.Deniose`.
    
    """
    def update(self):
        """Prints the current iteration."""
        print(self.iter, self.sprint_loss(),
              self.sprint_torch_min_max(self.subject.get_image_output_pt()))


class PrintBias(Print):
    """Prints the optimization for :class:`dip_bias.dip.BiasEstimate.
    
    """
    @property
    def level(self):
        """Returns the output level in a zero-padded format."""
        return pad_number(self.subject.level, Config.num_levels - 1)
    
    def update(self):
        """Prints the current iteration."""
        print(self.iter, self.level, self.sprint_loss(),
              self.sprint_torch_min_max(self.subject.get_bias_output_pt()),
              self.sprint_torch_min_max(self.subject.get_image_output_pt()))


class Writer:
    """Writes .csv file.

    Attributes:
        filename (str): The filename of the .csv.
        write_at_end (bool): Write the log after the optimization is finished.
        col_names (list[str]): The column names.
        formats (list[str]): The column formats, e.g. '%02d'.

    """
    def __init__(self, filename, col_names, formats, write_at_end=True):
        self.filename = filename
        self.write_at_end = write_at_end
        self.col_names= col_names
        self.formats = formats
        if self.write_at_end:
            self._lines = list()

    def setup(self):
        self._file = open(self.filename, 'w')
        self._write_header()

    def _write_header(self):
        """Writes the header."""
        line = ','.join(self.col_names) + '\n'
        if self.write_at_end:
            self._lines.append(line)
        else:
            self._file.write(line)
            self._file.flush()

    def write(self, values):
        line = [f % v for f, v in zip(self.formats, values)]
        line = ','.join(line) + '\n'
        if self.write_at_end:
            self._lines.append(line)
        else:
            self._file.write(line)
            self._file.flush()

    def cleanup(self):
        if self.write_at_end:
            self._file.write(''.join(self._lines))
        self._file.close()


class Log(Observer):
    """Logs the optimization.

    Attributes:
        filename (str): The filename of the .csv.
        write_at_end (bool): Write the log after the optimization is finished.
    
    """
    def __init__(self, filename, write_at_end=False):
        super().__init__()
        self.filename = filename
        self.write_at_end = write_at_end
        self._writer = self._init_writer()

    def _init_writer(self):
        raise NotImplementedError

    def setup(self):
        """Opens a file and write the header."""
        self._writer.setup()

    def cleanup(self):
        """Writes the log and/or closes the file."""
        self._writer.cleanup()


class LogDenoise(Log):
    """Logs the optimization for :class:`dip_bias.dip.Denoise`.
    
    """
    def _init_writer(self):
        col_names = ['iter', 'loss', 'min_im', 'max_im']
        formats = ['%d', '%f', '%f', '%f']
        return Writer(self.filename, col_names, formats, self.write_at_end)

    def update(self):
        """Logs the current iteration."""
        values = [self.subject.iter, self.subject.loss,
                  self.subject.get_image_output_pt().min().item(),
                  self.subject.get_image_output_pt().max().item()]
        self._writer.write(values)


class LogBias(Log):
    """Logs the optimization for :class:`dip_bias.dip.BiasEstimate`.
    
    """
    def _init_writer(self):
        col_names = ['iter', 'level', 'loss', 'min_bias',
                     'max_bias', 'min_im', 'max_im']
        formats = ['%d', '%d', '%f', '%f', '%f', '%f', '%f']
        return Writer(self.filename, col_names, formats, self.write_at_end)

    def update(self):
        """Logs the current iteration."""
        values = [self.subject.iter, self.subject.level, self.subject.loss,
                  self.subject.get_bias_output_pt().min().item(),
                  self.subject.get_bias_output_pt().max().item(),
                  self.subject.get_image_output_pt().min().item(),
                  self.subject.get_image_output_pt().max().item()]
        self._writer.write(values)


class SaveThread(threading.Thread):
    """Saves intermediate outputs as nifti images.

    Attributes:
        queue (queue.Queue): The queue to get images from the master thread. 
        affine (numpy.ndarray): The affine matrix of the image.
        header (nibabel.nifti1.Nifti1Header): The header of the image.
    
    """
    def __init__(self, queue, affine=np.eye(4), header=None):
        super().__init__()
        self.queue = queue
        self.affine = affine
        self.header = header

    def run(self):
        """Writes an intermediate output image."""
        while True:
            filename, data = self.queue.get()
            self.queue.task_done()
            if filename is None and data is None:
                break
            nii = nib.Nifti1Image(data.squeeze(), self.affine, self.header)
            nii.to_filename(filename)


class Save(Observer):
    """Saves outputs from :class:`dip_bias.dip.DIP` during iterations.

    Attributes:
        output_dir (str): The output directory.
        affine (numpy.ndarray): The affine matrix of the image.
        header (nibabel.nifti1.Nifti1Header): The header of the image.
        queue (queue.Queue): The queue to put intermediate data.
        thread (SaveThread): The thread to save data. It gets data from
            :attr:`queue`.
    
    """
    def __init__(self, output_dir, header=None, affine=np.eye(4)):
        super().__init__()
        self.output_dir = output_dir
        self.header = header
        self.affine = affine
        self.queue = Queue()
        self.thread = SaveThread(self.queue, affine, header)

    def setup(self):
        """Makes the output directory and starts the thread."""
        if len(self.output_dir) > 0 and not os.path.isdir(self.output_dir):
            os.makedirs(self.output_dir)
        self.thread.start()

    @property
    def _suffix(self):
        """Returns the filename suffix."""
        raise NotImplementedError

    @property
    def iter(self):
        """Returns the current iteration index in a zero-padded format."""
        return pad_number(self.subject.iter, Config.num_iters - 1)

    def _get_filename(self):
        """Gets the filename."""
        basename = '%s%s.nii.gz' % (self.iter, self._suffix)
        filename = os.path.join(self.output_dir, basename)
        return filename

    def _get_data(self):
        """Gets the data to save from :attr:subject."""
        raise NotImplementedError

    def update(self):
        """Puts the filename and data to the queue.

        The saving interval is set by
        :attr:`dip_bias.config.Config.save_interval.

        """
        if (self.subject.iter + 1) % Config.save_interval == 0:
            filename = self._get_filename()
            data = self._get_data()
            if data is not None:
                self.queue.put((filename, data))

    def cleanup(self):
        """Terminates the thread."""
        self.queue.join()
        self.queue.put((None, None))
        self.thread.join()


class SaveLevel(Save):
    def _get_filename(self):
        """Gets the filename."""
        basename = '%s_%s%s.nii.gz' % (self.iter, self.level, self._suffix)
        filename = os.path.join(self.output_dir, basename)
        return filename

    @property
    def level(self):
        """Returns the current level index in a zero-padded format."""
        return pad_number(self.subject.level, Config.num_levels - 1)


class SaveImage(Save):
    """Saves output images from :class:`dip_bias.dip.DIP` during iterations.

    Note:
        Use :class:`SaveImageInterm` for **unaveraged** images.

    """
    @property
    def _suffix(self):
        return '_image'

    def _get_data(self):
        return self.subject.get_image_output()


class SaveImageInterm(Save):
    """Saves the intermediate output images of :class:`dip_bias.dip.DIP`.

    """
    @property
    def _suffix(self):
        return '_image-interm'

    def _get_data(self):
        return self.subject.get_image_output_interm()


class SaveBias(Save):
    """Saves output bias from :class:`dip_bias.dip.BiasEstimate` during iters.

    Note:
        Use :class:`BiasIntermSave` to save **unaveraged** output bias fields.

    """
    @property
    def _suffix(self):
        return '_bias'

    def _get_data(self):
        return self.subject.get_bias_output()


class SaveBiasInterm(Save):
    """Saves intermediate bias fields from :class:`dip_bias.dip.BiasEstimate.

    """
    @property
    def _suffix(self):
        return '_bias-interm'

    def _get_data(self):
        return self.subject.get_bias_output_interm()


class SaveBiasLevel(SaveBias, SaveLevel):
    pass


class SaveBiasIntermLevel(SaveBiasInterm, SaveLevel):
    pass


class EvalFunc:
    """Abstract class for evaluation function (strategy)."""
    def eval(self, image, ref_image=None, mask=None):
        """Evaluates image.
        
        Args:
            image (numpy.ndarray): The first image.
            ref_image (numpy.ndarray): The second image.
            mask (numpy.ndarray): Only evaluate within mask (greater than 0).

        Returns:
            float: The evaluation result.

        """
        raise NotImplementedError


class MSE(EvalFunc):
    """Calculates MSE."""
    def eval(self, image, ref_image, mask=None):
        result = (image - ref_image) ** 2
        if mask is not None:
            result = result[mask > 0]
        result = np.mean(result)
        return result


class NMSE(EvalFunc):
    """Calculates normalized MSE.

    ..math ::

        \text{NMSE} = \frac{1}{N} \sum_{i=1}^N \left\lVert
                      \frac{a_i}{\sigma(a)_i} - \frac{b_i}{\sigma(b)_i}
                      \right\rVert_2^2

    """
    def eval(self, image, ref_image, mask=None):
        if mask is not None:
            mask = mask > 0
            sigma1 = np.std(image[mask])
            sigma2 = np.std(ref_image[mask])
        else:
            sigma1 = np.std(image)
            sigma2 = np.std(ref_image)
        n_image = image / sigma1
        n_ref_image = ref_image / sigma2
        return MSE().eval(n_image, n_ref_image, mask)


class NCC(EvalFunc):
    """Calculates normalized cross-correlation."""
    def eval(self, image, ref_image, mask=None):
        if mask is not None:
            mask = mask > 0
            image = image[mask]
            ref_image = ref_image[mask]
        dot_prod = np.mean(image * ref_image)
        std1 = np.std(image)
        std2 = np.std(ref_image)
        return dot_prod / (std1 * std2)


class NCC2(EvalFunc):
    """Calculates normalized cross-correlation."""
    def eval(self, image, ref_image, mask=None):
        if mask is not None:
            mask = mask > 0
            image = image[mask]
            ref_image = ref_image[mask]
        n_image = image - np.mean(image)
        n_ref_image = ref_image - np.mean(ref_image)
        dot_prod = np.mean(n_image * n_ref_image)
        std1 = np.std(image)
        std2 = np.std(ref_image)
        return dot_prod / (std1 * std2)


class CV(EvalFunc):
    """Calculates coefficient of variation."""
    def eval(self, image, ref_image=None, mask=None):
        image = np.abs(image)
        if mask is not None:
            mask = mask > 0
            image = image[mask]
        mean = np.mean(image)
        std = np.std(image)
        return std / mean


class CJV(EvalFunc):
    """Calculates coefficient of joint variation."""
    def eval(self, image, ref_image=None, mask=[None, None]):
        images = list()
        for m in mask:
            if m is not None:
                images.append(np.abs(image[m>0]))
            else:
                images.append(np.abs(image))
        std = np.std(images[0]) + np.std(images[1])
        meandiff = np.abs(np.mean(images[0]) - np.mean(images[1]))
        return std / meandiff


def create_eval_func(method):
    """Factory method to create an instance of :class:`EvalFunc`.

    Args:
        method (str): The name of the method.

    Returns:
        EvalFunc: The evaluation function.

    """
    if method == 'mse':
        return MSE()
    elif method == 'nmse':
        return NMSE()
    elif method == 'ncc':
        return NCC()
    elif method == 'ncc2':
        return NCC2()
    elif method == 'cv':
        return CV()
    elif method == 'cjv':
        return CJV()


class Eval(Observer):
    """Evaluates outputs of :class:`dip_bias.dip.DIP`.

    Attributes:
        filename (str): The filename of the output evaluation.
        ref_image (numpy.ndarray): The reference image.
        mask (numpy.ndarray): Evaluate the output within this mask.
        method (str): The evaluation method.
        write_at_end (bool): Write the evaluation after optimzation finished.
    
    """
    def __init__(self, filename, ref=None, mask=None, method='mse',
                 write_at_end=False):
        super().__init__()
        self.filename = filename
        self.ref = ref
        self.mask = mask
        self.method = method
        self.write_at_end = write_at_end

        self._eval_func = create_eval_func(self.method)
        self._writer = self._init_writer()

    def _init_writer(self):
        col_names = ['iter', self.method]
        formats = ['%d', '%f']
        return Writer(self.filename, col_names, formats, self.write_at_end)

    def setup(self):
        """Opens the output file and writes header."""
        self._writer.setup()

    def update(self):
        """Evaluates the output and writes the result into the file."""
        data = self._get_data()
        if data is not None:
            value = self._eval_func.eval(data, self.ref, self.mask)
            self._writer.write([self.subject.iter, value])

            # import matplotlib.pyplot as plt
            # fig = plt.figure()
            # num = 3
            # if isinstance(self.mask, list):
            #     num = 4
            #     plt.subplot(1, num, 1)
            #     plt.imshow(self.mask[0].squeeze())
            #     plt.subplot(1, num, 2)
            #     plt.imshow(self.mask[1].squeeze())
            # else:
            #     plt.subplot(1, num, 1)
            #     plt.imshow(self.mask.squeeze())
            # plt.subplot(1, num, num-1)
            # plt.imshow(data.squeeze())
            # plt.subplot(1, num, num)
            # plt.imshow(self.ref.squeeze())
            # fig.suptitle(self._eval_func.__class__.__name__ \
            #              + ' ' + self.__class__.__name__)
            # plt.show()

    def _get_data(self):
        """Gets the output data."""
        raise NotImplementedError

    def cleanup(self):
        """Writes the evaluation and closes the file."""
        self._writer.cleanup()


class EvalLevel(Eval):
    def _init_writer(self):
        col_names = ['iter', 'level', self.method]
        formats = ['%d', '%d', '%f']
        return Writer(self.filename, col_names, formats, self.write_at_end)

    def update(self):
        """Evaluates the output and writes the result into the file."""
        data = self._get_data()
        if data is not None:
            value = self._eval_func.eval(data, self.ref, self.mask)
            self._writer.write([self.subject.iter, self.subject.level, value])


class EvalImage(Eval):
    """Evaluates output images of :class:`dip_bias.dip.DIP`.

    Note:
        Use :class:`EvalImageInterm` to evaluate the **unaveraged** images.

    """
    def _get_data(self):
        return self.subject.get_image_output()


class EvalImageInterm(Eval):
    """Evaluates the intermediate outputs of :class:`dip_bias.dip.DIP`."""
    def _get_data(self):
        return self.subject.get_image_output_interm()


class EvalBias(Eval):
    """Evaluates output bias fields of :class:`dip_bias.dip.DIP`.

    Note:
        Use :class:`EvalBiasInterm` to evaluate the **unaveraged** images.

    """
    def _get_data(self):
        return self.subject.get_bias_output()


class EvalBiasInterm(Eval):
    """Evaluates the intermediate outputs of :class:`dip_bias.dip.DIP`."""
    def _get_data(self):
        return self.subject.get_bias_output_interm()


class EvalBiasLevel(EvalBias, EvalLevel):
    pass


class EvalBiasIntermLevel(EvalBiasInterm, EvalLevel):
    pass
